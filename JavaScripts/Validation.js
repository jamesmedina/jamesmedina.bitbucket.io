const email = document.getElementById('email')
const firstName = document.getElementById('first_name')
const lastName = document.getElementById('last_name')
const password1 = document.getElementById('password1')
const password2 = document.getElementById('password2')
const address = document.getElementById('address')
const phoneNumber = document.getElementById('phone_number')
const age = document.getElementById('age')
const form = document.getElementById('form')


form.addEventListener('submit', (e) => {
    e.preventDefault();
    checkInputs();
  })

  function checkInputs() {
    //check the value of all the inputs
    const emailVal = email.value.trim();
    const addressVal = address.value.trim();
    const firstNameVal = firstName.value.trim();
    const lastNameVal = lastName.value.trim();
    const phoneNumberVal = phoneNumber.value.trim();
    const ageVal = age.value.trim();
    const password1Val = password1.value.trim();
    const password2Val = password2.value.trim();
    const firstNameLength = firstName.value.length;
    const lastNameLength = lastName.value.length;
    const phoneNumberLength = phoneNumber.value.length;

    if(emailVal === '') {

      setErrorFor(email, 'Field is Blank');
    } else if(!validEmail(emailVal)) {
        setErrorFor(email, 'Email Is Invalid');
    } else {
      setSuccessFor(email)
    }

    if(addressVal === '') {

      setErrorFor(address, 'Field is Blank');
    }

    else {

      setSuccessFor (address);

    }

    if(firstNameVal === '') {
      //show the error here
      setErrorFor(firstName, 'Field is Blank');
    }

    else if (firstNameLength <= 1) {
      setErrorFor (firstName, 'First Name Is Too Short');
    }

    else if (firstNameLength >= 20) {
      setErrorFor (firstName, 'First Name Is Too Long');
    }

    else {

      setSuccessFor (firstName);

    }

    if(lastNameVal === '') {

      setErrorFor(lastName, 'Field is Blank');
    }

    else if (lastNameLength <= 1) {
      setErrorFor(lastName, 'Last Name Is Too Short');
    }

    else if (lastNameLength >= 20) {
      setErrorFor(lastName, 'Last Name Is Too Long');
    }

    else {

      setSuccessFor (lastName);

    }
    if(phoneNumberVal === '') {

      setErrorFor(phoneNumber, 'Field is Blank');
    }

    else if (phoneNumberLength == 9) {
      setErrorFor(phoneNumber, 'Please Enter Country Code')
    }

    else if (phoneNumberLength < 9) {
      setErrorFor(phoneNumber, 'Invalid Phone Number')
    }
    else if (phoneNumberLength > 12) {
      setErrorFor (phoneNumber, 'Invalid Phone Number')
    }

    else if (!validPhone(phoneNumberVal)){
      setErrorFor(phoneNumber, 'Please Enter An Australian Phone Number');

    }
    else {
      setSuccessFor(phoneNumber);
    }

    if (ageVal === ''){
      setErrorFor(age, 'Field Is Blank');
    }
    else if (ageVal <= 13) {
      setErrorFor(age, "You are too young");
    }
    else if (ageVal >= 50) {
      setLinkFor(age);
    }

    else {
      setSuccessFor (age);
    }

    if(password1Val === '') {

      setErrorFor(password1, 'Field is Blank');
    }
    else if (password2Val != password1Val) {
      setErrorFor (password1, "Passwords Are Not Matching");
      setErrorFor (password2, "Passwords Are Not Matching");
    }

    else {

      setSuccessFor (password1);

    }


    if(password2Val === '') {

      setErrorFor(password2, 'Field is Blank');
    }

    else if (password2Val != password1Val) {
      setErrorFor (password1, "Passwords Are Not Matching");
      setErrorFor (password2, "Passwords Are Not Matching");
    }

    else {

      setSuccessFor (password2);

    }
  }

  function setErrorFor(input, message) {
      const controller = input.parentElement;
      const small = controller.querySelector('small');

      small.innerText = message;

      controller.className = 'input error';
    }

  function setSuccessFor(input) {
      const controller = input.parentElement;
      controller.className = 'input success';
    }

  function setLinkFor(input) {
      const controller = input.parentElement;
      controller.className = 'input link';
    }






  //function to check if email is valid using regular expression.
    function validEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}

function validPhone (phoneNumber) {
  return /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/.test(phoneNumber);
}
